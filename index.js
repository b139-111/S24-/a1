//1st
db.users.find(
	{
		$or:[
			 {firstName:{$regex: 's', $options: 'i'}},
             {lastName:{$regex: 'd', $options: 'i'}}
		]
	},
	{firstName:1,lastName:1,_id:0}
);

//2nd
db.users.find(
	{
		$and:[
			 {department: "HR"},
             {age: {$gte:70}}
		]
	}
);

//3rd
db.users.find(
	{
		$and:[
			 {firstName:{$regex: 'e', $options: 'i'}},
             {age: {$lte:30}}
		]
	}
);



////////////data
db.users.insertMany([
{
    "_id" : ObjectId("618b9e79b3e8a398f53f1251"),
    "firstName" : "Bill",
    "lastName" : "Gates",
    "age" : 65.0,
    "contact" : {
        "phone" : "123456789",
        "email" : "bill@mail.com"
    },
    "courses" : [ 
        "PHP", 
        "Laravel", 
        "HTML"
    ],
    "department" : "Operations"
},

{
    "_id" : ObjectId("618ba4b3b3e8a398f53f1252"),
    "firstName" : "Billy",
    "lastName" : "Crawford",
    "age" : 35.0,
    "contact" : {
        "phone" : "123456789",
        "email" : "billycrawford@mail.com"
    },
    "courses" : [ 
        "React", 
        "Node", 
        "Express"
    ],
    "department" : "Finance"
},

{
    "_id" : ObjectId("618bb2e2b3e8a398f53f1254"),
    "firstName" : "Jane",
    "lastName" : "Doe",
    "age" : 21.0,
    "contact" : {
        "phone" : "87654321",
        "email" : "janedoe@mail.com"
    },
    "courses" : [ 
        "CSS", 
        "JavaScript", 
        "Python"
    ],
    "department" : "HR"
},

{
    "_id" : ObjectId("618bb2e2b3e8a398f53f1255"),
    "firstName" : "Stephen",
    "lastName" : "Hawking",
    "age" : 76.0,
    "contact" : {
        "phone" : "87654321",
        "email" : "stephenhawking@mail.com"
    },
    "courses" : [ 
        "Python", 
        "React", 
        "PHP"
    ],
    "department" : "HR"
},

{
    "_id" : ObjectId("618bb2e2b3e8a398f53f1256"),
    "firstName" : "Neil",
    "lastName" : "Armstrong",
    "age" : 82.0,
    "contact" : {
        "phone" : "87654321",
        "email" : "neilarmstrong@mail.com"
    },
    "courses" : [ 
        "React", 
        "laravel", 
        "Sass"
    ],
    "department" : "HR"
}
]);
///////////////